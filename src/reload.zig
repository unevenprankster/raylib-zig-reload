const std = @import("std");
const DynLib = std.DynLib;
const Dir = std.fs.Dir;

const Self = @This();

const funcptr = *const fn() void;
const lib_name = "library.dll";
const lib_load_name = "library.loaded.dll";

dir_handle : Dir = undefined,

lib : DynLib = undefined,

init: funcptr = undefined,
update: funcptr = undefined,

last_mod : i128 = 0,

pub fn startup(self: *Self, comptime path: []const u8) !void {
    self.dir_handle = try std.fs.cwd().openDir(path, .{});
    
    try Dir.copyFile(self.dir_handle, lib_name, self.dir_handle, lib_load_name, .{});
    
    self.lib = try DynLib.open(path ++ lib_load_name);
    
    self.init = self.lib.lookup(funcptr, "init") orelse return error.SymbolNotFound;
    self.update = self.lib.lookup(funcptr, "update") orelse return error.SymbolNotFound;
    
    self.last_mod = last_mod: {
        const stat = try self.dir_handle.statFile(lib_name);
        break :last_mod stat.mtime;
    };
    
    self.init();
}

pub fn watch(self: *Self, comptime path: []const u8) !void {
    self.update();
    
    const stat = try self.dir_handle.statFile(lib_name);
    if(stat.mtime == self.last_mod)
        return;
    
    self.last_mod = stat.mtime;
    
    self.lib.close();
    try Dir.copyFile(self.dir_handle, lib_name, self.dir_handle, lib_load_name, .{});
    self.lib = try DynLib.open(path ++ lib_load_name);
    self.update = self.lib.lookup(funcptr, "update") orelse return error.SymbolNotFound;
}

pub fn finish(self: *Self) void {
    self.lib.close();
}