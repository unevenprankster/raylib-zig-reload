const raylib = @import("shared.zig").raylib;

const std = @import("std");

export fn init() void{
    std.debug.print("Apples.\n", .{});
}

export fn update() void{
    raylib.BeginDrawing();
    defer raylib.EndDrawing();
    raylib.ClearBackground(raylib.RAYWHITE);
    raylib.DrawText("9001% free-range dynamic function", 100, 100, 20, raylib.BLACK);
}