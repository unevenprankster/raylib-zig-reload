const raylib = @import("shared.zig").raylib;
const std = @import("std");

const Reloader = @import("reload.zig");
var lib: Reloader = undefined;

pub fn main() !void {
    raylib.InitWindow(640, 480, "Laserpit");
    raylib.SetConfigFlags(raylib.FLAG_WINDOW_RESIZABLE);
    raylib.SetTargetFPS(60);

    defer raylib.CloseWindow();
    
    try lib.startup("zig-out/lib/");
    defer lib.finish();
    
    while (!raylib.WindowShouldClose()) {
        try lib.watch("zig-out/lib/");
    }
}
