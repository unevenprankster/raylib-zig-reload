# raylib-zig-reload

A Windows-focused proof of concept for hot reloading a Raylib game built with Zig

## How to run

1. Run `zig build library`
2. Run `zig build run`
3. Open `lib.zig`, make a change, then run `zig build library`
4. It should reload! Yippie!

## TODO:

- Why is `zig build library` running so slow in my computer? Windows trickery? `cl` runs fast...
- Implement some method to save state (maybe a block of memory passed to `init()`...)