const std = @import("std");
const raylib = @import("raylib/src/build.zig");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "reload",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    
    exe.addIncludePath("raylib/src");
    
    const rl = raylib.addRaylib(b, target, optimize);
    exe.linkLibrary(rl);
    
    exe.install();

    const run_cmd = exe.run();

    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
    
    const lib = b.addSharedLibrary(.{
        .name = "library",
        .root_source_file = .{ .path = "src/lib.zig" },
        .target = target,
        .optimize = optimize,
    });
    
    lib.addIncludePath("raylib/src");
    
    lib.linkLibrary(rl);
    lib.install();
    
    const lib_step = b.step("library", "Compile the shared lib");
    lib_step.dependOn(&b.addInstallArtifact(lib).step);
}
